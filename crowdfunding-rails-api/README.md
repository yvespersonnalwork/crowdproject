gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 \ 7D2BAF1CF37B13E2069D6956105BD0E739499BDB  
curl -sSL https://get.rvm.io | bash -s stable --ruby  
source /home/vagrant/.rvm/scripts/rvm  
rvm get stable --autolibs=enable  
rvm list known  
rvm install ruby-2.5.1  
ruby -v  
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -  
sudo apt install -y nodejs  
sudo apt install gcc g++ make  
gem update --system  
gem -v  
echo "gem: --no-document" >> ~/.gemrc  
gem install rails -v 5.2.0  
rails -v  
sudo apt-get install libpq-dev  
bundle install  


sudo apt install postgresql postgresql-contrib libpq-dev -y  
systemctl start postgresql   
systemctl enable postgresql  
sudo su  
su - postgres  
psql  
\password postgres  
create role vagrant with createdb login password 'vagrant';  
\du  
rails db:create
rails db:setup
rails db:migrate
rails s -p 3000 -b 0.0.0.0  
