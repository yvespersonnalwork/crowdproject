import { environment } from './../environments/environment';
import { CustomConfig } from 'ng2-ui-auth';


const apiLink: string = environment.API_ENDPOINT; // "http://localhost:3000";
export const GOOGLE_CLIENT_ID = '863762181445-gftq46jf3cokccmjp5mmn1ljvau9249q.apps.googleusercontent.com';
export const FACEBOOK_CLIENT_ID = '801579219992920';

export class MyAuthConfig extends CustomConfig {
  defaultHeaders = { 'Content-Type': 'application/json' };
  providers = {
    google: { clientId: GOOGLE_CLIENT_ID, url: `${apiLink}/api/v1/auth/google` },
    facebook: { clientId: FACEBOOK_CLIENT_ID, url: `${apiLink}/api/v1/auth/facebook` }
  };
}
