export const environment = {
  production: true,
  token_auth_config: {
  apiBase: 'http://192.168.50.179:3000'
  },
  API_ENDPOINT: 'http://192.168.50.179:3000',
  UI_ENDPOINT: 'http://192.168.50.179:4200',
  AppName: 'CrowdPouch'
};
